# makefile-control-functions-demo

This is a small demo of the three available functions that control make.

* error
* warning
* info

See the [Makefile](./Makefile).

For the full documentation on this,
see [8.13 Functions That Control Make](https://www.gnu.org/software/make/manual/html_node/Make-Control-Functions.html)
