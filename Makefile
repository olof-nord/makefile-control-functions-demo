# see https://www.gnu.org/software/make/manual/html_node/Make-Control-Functions.html

all: info-message warning-message error-message

.PHONY: error-message
error-message:
	$(error This is a fatal error, an error is generated!)

.PHONY: warning-message
warning-message:
	$(warning This is a warning message, the execution will not exit.)

.PHONY: info-message
info-message:
	$(info This is printing a normal message.)
